import React, { useState } from 'react';
import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';

import HotelsList from '../../components/HotelsList';
import ConfirmModal from '../../components/ConfirmModal';
import Constants from '../../constants';


const NearHotelsMap = ({ google: googleMaps }) => {
  const [selectedPlace, setSelectedPlace] = useState({});
  const [isShowBookingModal, setIsShowBookingModal] = useState(null);
  const mapRef = React.useRef(null)
  const [location, setLocation] = useState({
    lat: 48.135124,
    lng: 11.581981,
  });
  const [places, setPlaces] = useState([]);

  const centerMoved = (mapProps, map) => {
    searchNearby(map, map.center);
  };

  const setUserLocation = position => {
    const coordinates = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
    }
    setLocation(coordinates)
    mapRef.current.map?.panTo(coordinates);
    centerMoved(mapRef.current.props, mapRef.current.map);
  }

  React.useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      setUserLocation,
      function(error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      }
    );
  });

  const searchNearby = (map, center) => {
    const service = new googleMaps.maps.places.PlacesService(map);

    const request = {
      location: center,
      radius: 200 * 1000,
      type: [Constants.PLACE_TYPE]
    };

    service.nearbySearch(request, (results, status) => {
      if (status === googleMaps.maps.places.PlacesServiceStatus.OK) {
        setPlaces(results);
      }
    });
  };
  const onMapReady = (mapProps, map) => {
    searchNearby(map, map.center);
  }

  const onMarkerClick = (props, placeId) => {
    setSelectedPlace({
      ...props,
      placeId
    })
  }

  const getMarkerIconUrl = place => {
    if (place.place_id === selectedPlace.placeId) {
      return Constants.ACTIVE_MARKER_ICON;
    }
    return Constants.DEFAULT_MARKER_ICON;
  }

  const handleBookingOpen = (hotel) => setIsShowBookingModal(hotel);
  const handleBookingClose = () => setIsShowBookingModal(null);

  const renderHotelsList = () => (
    <HotelsList
      hotels={places}
      selectedPlace={selectedPlace}
      handleBookingOpen={handleBookingOpen}
      handleBookingClose={handleBookingClose}
    />
  );
  const renderConfirmModal = () => (
    <ConfirmModal handleClose={handleBookingClose}>
      <div>
        Are you sure?
      </div>
      <div>
        TODO: cancel and confirm button
      </div>
    </ConfirmModal>
  );

  return (
    <>
      {isShowBookingModal && renderConfirmModal()}
      <Map
        ref={mapRef}
        initialCenter={location}
        fullscreenControl={false}
        mapTypeControl={false}
        google={googleMaps} 
        zoom={14}
        onReady={onMapReady}
        onDragend={centerMoved}
      >

        {places.map(place => (
          <Marker 
            key={place.place_id}
            position={{
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng(),
            }}
            icon={{
              url: getMarkerIconUrl(place),
              scaledSize: googleMaps.maps.Size(24, 24)
            }}
            onClick={(props) => onMarkerClick(props, place.place_id)}
            name={place.name} />
        ))}
        {places.length && renderHotelsList()}
      </Map>
    </>
  );
}

export default GoogleApiWrapper({
    apiKey: process.env.REACT_APP_GOOGLE_KEY
  })(NearHotelsMap)
