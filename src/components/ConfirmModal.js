import React from 'react'
import PropTypes from 'prop-types';

import * as S from './style';

const ConfirmModal = ({ children, handleClose }) => {
  return (
    <div>
      <S.Modal>
        {children}
      </S.Modal>
      <S.Background onClick={handleClose} />
    </div>
  )
}

ConfirmModal.propTypes = {
  children: PropTypes.node.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default ConfirmModal;
