
import React from 'react';
import { render, screen, cleanup } from '@testing-library/react';

import HotelCard from '../HotelCard';

afterEach(cleanup);


describe('HotelCard component tests', () => {
  let hotel, selectedPlace, handleBookingOpen;

  beforeEach(() => {
    hotel =  {
        "name": "Cortiina Hotel",
        "photos": [
            {
              getUrl: () => "https://img.icons8.com/ios/452/cancel-2.png"
            }
        ],
        "place_id": "ChIJZyLWSop1nkcRnQW0FofT8bU",
        "reference": "ChIJZyLWSop1nkcRnQW0FofT8bU",
        "vicinity": "Ledererstraße 8, München"
    };;
    selectedPlace = {};
    handleBookingOpen = jest.fn();
  });

  it('should render properly', () => {
    render(
      <HotelCard 
        key={hotel.reference}
        hotel={hotel}
        selectedPlace={selectedPlace}
        handleBookingOpen={handleBookingOpen}
      />
    );
    const vicinity = screen.getByText(/Ledererstraße 8, München/i);
    const name = screen.getByText(/Cortiina Hotel/i);

    expect(vicinity).toBeInTheDocument();
    expect(name).toBeInTheDocument();
  });

  it('shouldnt render if you not give a hotel object', () => {
    const consoleSpy = jest
      .spyOn(console, 'error')
      .mockImplementation(() => {});

    expect(() => render(
      <HotelCard 
        key={hotel.reference}
        hotel={{}}
        selectedPlace={selectedPlace}
        handleBookingOpen={handleBookingOpen}
      />
    )).toThrow(TypeError);

    expect(consoleSpy).toHaveBeenCalled();
  });
});
