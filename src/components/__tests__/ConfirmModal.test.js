
import React from 'react';
import { render, screen, cleanup } from '@testing-library/react';

import ConfirmModal from '../ConfirmModal';

afterEach(cleanup);


describe('ConfirmModal component tests', () => {
  let handleClose;

  beforeEach(() => {
    handleClose = jest.fn();
  });

  it('checking ConfirmModal component with children props', () => {
    const bodyText = 'confirm body test';

    render(
      <ConfirmModal handleClose={handleClose}>
        <span>{bodyText}</span>
      </ConfirmModal>
    );
    expect(screen.getByText(bodyText)).not.toBeUndefined();
  });

  it('shouldnt render if you not give a children', () => {
    const consoleSpy = jest
      .spyOn(console, 'error')
      .mockImplementation(() => {});

    render(
      <ConfirmModal handleClose={handleClose} />
    );

    expect(consoleSpy).toHaveBeenCalled();
  });

});