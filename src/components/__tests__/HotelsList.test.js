
import React from 'react';
import { render, screen, cleanup } from '@testing-library/react';

import HotelsList from '../HotelsList';

afterEach(cleanup);


describe('HotelsList component tests', () => {
  let hotels, selectedPlace, handleBookingOpen;

  beforeEach(() => {
    hotels = [
      {
        "name": "Cortiina Hotel",
        "photos": [
          {
              getUrl: () => "https://img.icons8.com/ios/452/cancel-2.png"
          }
        ],
        "place_id": "ChIJZyLWSop1nkcRnQW0FofT8bU",
        "reference": "ChIJZyLWSop1nkcRnQW0FofT8bU",
        "vicinity": "Ledererstraße 8, München"
      },
      {
        "name": "Bla Bla Hotel",
        "photos": [
          {
              getUrl: () => "https://img.icons8.com/ios/452/cancel-2.png"
          }
        ],
        "place_id": "ChIJZyLWSop1nkcRnQW0FofT8bU123",
        "reference": "ChIJZyLWSop1nkcRnQW0FofT8bU123",
        "vicinity": "vicinity 8, München"
      }
    ];
    selectedPlace = {};
    handleBookingOpen = jest.fn();
  });

  it('should render hotels properly', () => {
    render(
      <HotelsList 
        hotels={hotels}
        selectedPlace={selectedPlace}
        handleBookingOpen={handleBookingOpen}
      />
    );
    const hotel1 = screen.getByText(/Bla Bla Hotel/i);
    const hotel2 = screen.getByText(/Cortiina Hotel/i);

    expect(hotel1).toBeInTheDocument();
    expect(hotel2).toBeInTheDocument();
  });
});
