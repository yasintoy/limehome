import React from 'react';
import PropTypes from 'prop-types';

import HotelCard from './HotelCard';
import * as S from './style';

const HotelsList = ({ 
  hotels,
  selectedPlace,
  handleBookingOpen,
 }) => {
  return (
    <S.Container>
      {hotels.map(hotel => (
        <HotelCard 
          key={hotel.reference}
          hotel={hotel}
          selectedPlace={selectedPlace}
          handleBookingOpen={handleBookingOpen}
        />
      ))}
    </S.Container>
  )
}

HotelsList.propTypes = {
  hotels: PropTypes.array.isRequired,
  selectedPlace: PropTypes.object,
  handleBookingOpen: PropTypes.func.isRequired,
};

export default HotelsList;
