import styled, { css } from "styled-components";

const Block = styled.div`
  height: 250px;
  flex: 0 0 400px;
  background-color: #ffff;
  margin-left: 10px;
  z-index: 100;

  ${props => props.isSelected && css`
    border: 2px solid orange;
  `
  }
`;

const InfoContainer = styled.div`
  display: flex;
  padding: 10px 10px;
`;

const HotelInfo = styled.div`
  display: grid;
  grid-template-rows: 0fr 1fr 1fr;
  gap: 10px;
  grid-auto-rows: 100px;
`;

const CardTitle = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 16px;
  color: #191919;
  background-color: #fff;
  padding: 10px 10px;
`;

const CardImage = styled.div`
  padding: 10px 10px;
`

const CardVicinity = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 16px;
  color: #bdc1c5;
  background-color: #fff;
  padding: 5px 10px;
`;

const CardPrice = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 24px;
  color: #323232;
  background-color: #fff;
  padding: 15px 10px;
`;

const BookButton = styled.button`
  margin-top: 5px;
  height: 40px;
  width: 100%;
  border-style: none;
  color: #ffff;
  background-color: #f6941c;
`;


const Container = styled.div`
  display: flex;
  overflow-x: auto;
  position: fixed;
  margin-bottom: 10px;

  bottom: 0;
  left: 0;
  right: 0;
`;

/*
  if it's a real life project, it should be split into new file
  components/
   * Modal/
    -Confirm
      index.js
      style.js
  * Hotel/
      ....
*/

const Modal = styled.div`
  z-index: 9999;
  position: absolute;
  top: 50%;
  left: 50%;
  min-width: 250px;
  min-height: 100px;
  padding: 15px;
  border-radius: 10px;
  transform: translate(-50%, -50%);
  background: #fff;
`;

const Background = styled.div`
  z-index: 9998;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background: rgba(0,0,0,0.3);
`;

export {
  Container,
  Block,
  InfoContainer,
  HotelInfo,
  CardTitle,
  CardImage,
  CardVicinity,
  CardPrice,
  BookButton,
  Modal,
  Background,
}