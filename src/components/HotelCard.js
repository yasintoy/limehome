import React from 'react'
import PropTypes from 'prop-types';

import * as S from './style';

const HotelCard = ({ 
  hotel,
  selectedPlace,
  handleBookingOpen,
 }) => {
  const ref = React.useRef();

  const getImgUrl = () => {
    if (!hotel.photos) return 'https://img.icons8.com/ios/452/cancel-2.png'; // todo: unavailable img url move into folder
    return hotel.photos[0].getUrl();
  }

  React.useEffect(() => {
    if (selectedPlace.placeId === hotel.reference && ref.current) {
      ref.current.scrollIntoView({ behavior: "smooth" });
    }
  }, [selectedPlace, hotel]);

  return (
    <S.Block ref={ref} isSelected={selectedPlace.placeId === hotel.reference}>
      <S.InfoContainer>
        <S.CardImage>
          <img src={getImgUrl()} height={150} width={150} alt={`${hotel.name}`} />
        </S.CardImage>
        <S.HotelInfo>
          <S.CardTitle>
            {hotel.name}
          </S.CardTitle>
          <S.CardVicinity>
            {hotel.vicinity}
          </S.CardVicinity>
          <S.CardPrice>
            €98
          </S.CardPrice>
        </S.HotelInfo>
      </S.InfoContainer>
      <S.BookButton onClick={() => handleBookingOpen(hotel)}>
        Book
      </S.BookButton>
    </S.Block>
  )
}

HotelCard.propTypes = {
  hotel: PropTypes.object.isRequired,
  selectedPlace: PropTypes.object,
  handleBookingOpen: PropTypes.func.isRequired,
};

export default HotelCard;