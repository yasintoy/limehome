import React from 'react';

import Header from '../layout/Header'
import Body from '../layout/Body'
import NearHotelsMap from '../containers/Map/index'

const NearHotels = () => (
  <>
    <Header />
    <Body>
      <NearHotelsMap />
    </Body>
  </>
);

export default NearHotels;
