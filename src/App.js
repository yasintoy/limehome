import React from 'react';

import { GlobalStyles } from './global';
import NearHotels from './pages/NearHotels';

// There's only one page so I didn't use react router here
const App = () => (
  <>
    <GlobalStyles />
    <NearHotels/>
  </>
);

export default App;
