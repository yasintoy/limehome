const Constants =  {
  PLACE_TYPE: 'lodging',
  ACTIVE_MARKER_ICON: '/home-icon-active.svg',
  DEFAULT_MARKER_ICON: '/home-icon.svg',
}

export default Constants;

