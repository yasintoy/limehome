import styled from "styled-components";

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #ebeae5;
  height: 50px;

  @media (min-width: 768px){
    justify-content: center;
  }
`
const Logo = styled.div`
  padding-left: 10px;
`
const BurgerIcon = styled.div`
  padding-right: 10px;

  @media (min-width: 768px){
    display: none;
  }
`;

export {
  Header,
  BurgerIcon,
  Logo,
}