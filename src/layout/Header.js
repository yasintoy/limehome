import React from 'react';

import * as S from './style';

const Header = () => (
  <S.Header>
    <S.Logo>
      <img height="20px" src="/logo.svg" alt="LimeHome Logo" />
    </S.Logo>
    <S.BurgerIcon>
      <img height="20px" src="/burger-icon.svg" alt="LimeHome Logo" />
    </S.BurgerIcon>
  </S.Header>
);

export default Header;
